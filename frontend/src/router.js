import Vue from 'vue';
import Router from 'vue-router';
import Login from './components/Login';
import Register from './components/Register';
import Profile from './components/Profile';

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
      {
        path: '/login',
        name: 'login',
        component: Login
      },
      {
        path: '/register',
        name: 'register',
        component: Register
      },
      {
        path: '/',
        name: 'home',
        component: Login
      },
      {
        path: '/profile',
        name: 'profile',
        component: Profile
      },
    ]
});