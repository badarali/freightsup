import Vue from 'vue'
import App from './App.vue'
import router from './router'
import notify from "bootstrap-notify";

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
