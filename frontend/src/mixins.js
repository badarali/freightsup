export default {
    data: function(){
        return{
            ip: "http://localhost:8000/",
            islogin: false,
            role: "",
            accesstoken: localStorage.getItem("accesstoken"),
            user_id: localStorage.getItem("user_id"),
            version: "",
            header: {
                crossdomain: true,
                headers: {
                "Authorization": "Bearer " + localStorage.getItem("token"),
                "Content-Type": "application/json",
                "Accept": "application/json",
                }
            },
        }
    },
    methods: {
        notifyAdded: function(){
            $.notify({
                message: 'New Data Added',
            },{
                type: 'success'
            });
        },
        notifyCustom: function(message, type){
            $.notify({
                message: message,
            },{
                type: type
            });
        }
    }
}